Kyle R. Conway is a Vice President of Operations for a non-profit, a former Director of Training and Curriculum at a behavioral health organization, a recovering University Graduate part-time Art Instructor, and a perpetually afflicted multidiscriplinary artist.

Kyle holds a PhD in Fine Arts from Texas Tech University.

He likes tea.